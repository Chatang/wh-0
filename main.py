import pandas
from sklearn import model_selection
from apyori import apriori

def load_data(filename):
    data = pandas.read_csv(filename)
    return data

#print(data)

item_list = data.unstack().dropna().unique()
print(item_list)
print("# item:", len(item_list))

train,test = model_selection.train_test_split(data, test_size = .10)
print("test:",test)
print("train:",train)

train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
print("transform")
print(train)

result = list(apriori(train, min_support = 0.0045, min_confidence = 0.2, min_lift = 3, min_length = 2))
print("result")
print(result)

for rule in result:
    pair=rule[0]
    components = [i for i in pair]
    print(pair)
    print("Rule:", components[0].'->',components[1])
    print('')

